import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import { Container, Item, Form, Input, Button, Label, Header, Content, ListItem, Radio, Right, Left } from "native-base";
import ValidationComponent from 'react-native-form-validator';
import ModalSelector from 'react-native-modal-selector';

import {
  Image,
  Platform,
  SafeAreaView, 
  ScrollView,
  StyleSheet,
  Text,
  Picker,
  TouchableOpacity,
  View,
  AsyncStorage
} from 'react-native';
//import { TextInputMask } from "react-native-masked-text";
import TextInputMask from '../lib/text-input-mask'

import firebase from "@firebase/app"
import "firebase/auth"
import "firebase/database"

export default class SignUpScreen extends ValidationComponent {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            selectedFederacao: "Qual Federação ou Obediência?",
            selectedAtivo: "Você está ativo na Ordem?",
            nome_completo: "",
            nome_tratamento: "",
            loja: "",
            nome_loja: "",
            oriente: "",
            potencia: "",
            numero_cim_ordem: "",
            celular: "",
        };
        this.Input = "";
    }

    componentWillMount() {
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            await AsyncStorage.getItem('user', (err, user) => {
                console.log("_retrieveData");
                console.log(user);
                console.log(typeof user);
                console.log(user !== null && typeof user !== "string");
                if (user !== null && typeof user !== "string") {
                    this.props.navigation.navigate('App');
                }
            });
        } catch (error) {
            // Error retrieving data
        }
    };

    especialCharMask (especialChar){
        especialChar = especialChar.replace('/[áàãâä]/ui', 'a');
        especialChar = especialChar.replace('/[éèêë]/ui', 'e');
        especialChar = especialChar.replace('/[íìîï]/ui', 'i');
        especialChar = especialChar.replace('/[óòõôö]/ui', 'o');
        especialChar = especialChar.replace('/[úùûü]/ui', 'u');
        especialChar = especialChar.replace('/[ç]/ui', 'c');
        especialChar = especialChar.replace('/[^a-z0-9]/i', '_');
        especialChar = especialChar.replace('/_+/', '_'); //
        especialChar = especialChar.replace('-', ''); //
        especialChar = especialChar.replace('(', ''); //
        especialChar = especialChar.replace(')', ''); //
        especialChar = especialChar.replace(' ', ''); //
        return especialChar;
    }

    SignUp(email, password){
        console.log("SignUp");
        try {
            firebase.auth().createUserWithEmailAndPassword(email, password).then(res => {
                firebase.auth().onAuthStateChanged(user => {
                    if(user){
                        var defaultDatabase = firebase.database();
                        var perfilRef = defaultDatabase.ref("perfil");

                        var uid = user.uid;
                        var query = perfilRef.orderByChild('uid').equalTo(uid).limitToFirst(1);
                        query.once('value', snap =>{
                            if(snap.val() == null){
                                var id = perfilRef.push({
                                    selectedFederacao: this.state.selectedFederacao,
                                    selectedAtivo: this.state.selectedAtivo,
                                    nome_completo: this.state.nome_completo,
                                    nome_tratamento: this.state.nome_tratamento,
                                    loja: this.state.loja,
                                    nome_loja: this.state.nome_loja,
                                    oriente: this.state.oriente,
                                    potencia: this.state.potencia,
                                    numero_cim_ordem: this.state.numero_cim_ordem,
                                    celular: "+55" + this.especialCharMask(this.state.celular),
                                    uid: user.uid,
                                    aprovado: false,
                                    startedAt: firebase.database.ServerValue.TIMESTAMP
                                }).getKey();
                            }
                            let storageUser = {
                                uid: user.uid,
                                email: user.email,
                                photoURL: user.photoURL
                            }
                            //AsyncStorage.setItem('user', JSON.stringify(storageUser));
                            this.props.navigation.navigate('AuthLoading');
                            alert("Seu cadastro foi enviado para aprovação. Entraremos em contato via e-mail/SMS informando a aprovação.");
                        });
                    }
                })    
            }).catch(error =>{
                alert(error.toString(error));
            });
        } catch (error) {
            console.log(error.toString(error));
        }
    };

    Back(){
        this.props.navigation.navigate('AuthLoading');
    }

    render() {
        const dataFederacao = [
            { key: 1, label: "GOB (Grandes Orientes Estaduais)"},
            { key: 2, label: "CMSB (Grandes Lojas)"},
            { key: 3, label: "COMAB (Grandes Orientes Independentes)"},
            { key: 4, label: "UGLE (Grande Loja Unida da Inglaterra)"},
            { key: 5, label: "Outro" }
        ];
        const dataAtivo = [
            { key: 1, label: "Ativo"},
            { key: 2, label: "Quite Placet / Certificado de Grau / Adormecido"},
            { key: 3, label: "Outro"}
        ];
        return (
        <Container style={styles.container}>
        <SafeAreaView>
            <ScrollView style={styles.scrollStyle}>
                
                        <Form>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Nome completo do irmão</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={nome_completo => this.setState({ nome_completo })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Como prefere ser chamado?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={nome_tratamento => this.setState({ nome_tratamento })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual o nome da sua loja?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={loja => this.setState({ loja })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual o número?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={nome_loja => this.setState({ nome_loja })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual o oriente?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={oriente => this.setState({ oriente })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual a Potência Maçônica?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={potencia => this.setState({ potencia })}
                                />
                            </Item>
                            <View style={{marginTop: 20, marginBottom: 5}}>
                                <Label style={styles.label}>Federação</Label>
                                <ModalSelector
                                data={dataFederacao}
                                cancelText="Cancelar"
                                touchableStyle={styles.dropdown}
                                selectStyle={styles.childrenContainerStyle}
                                childrenContainerStyle={styles.childrenContainerStyle}
                                initValue={this.state.selectedFederacao}
                                onChange={(option)=>{ this.setState({ selectedFederacao: option.label})}} />
                            </View>
                            <View style={{marginTop: 20, marginBottom: 0}}>
                                <Label style={styles.label}>Status</Label>
                                <ModalSelector
                                data={dataAtivo}
                                cancelText="Cancelar"
                                touchableStyle={styles.dropdown}
                                selectStyle={styles.childrenContainerStyle}
                                childrenContainerStyle={styles.childrenContainerStyle}
                                initValue={this.state.selectedAtivo}
                                onChange={(option)=>{ this.setState({ selectedAtivo: option.label})}} />
                            </View>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual seu número de cadastro ou CIM na Ordem?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={numero_cim_ordem => this.setState({ numero_cim_ordem })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual o número do seu CELULAR com DDD?</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={celular => this.setState({ celular })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Endereço de e-mail</Label>
                                <Input 
                                    autoCapitalize="none" 
                                    autoCorrect={false} 
                                    style={styles.textInput}
                                    onChangeText={email => this.setState({ email })}
                                />
                            </Item>
                            <Item floatingLabel style={styles.itemFloat}>
                                <Label style={styles.label}>Qual a SENHA que você quer cadastrar para utilizar o Bro3? </Label>
                                <Input
                                    secureTextEntry={true}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    style={styles.textInput}
                                    onChangeText={password => this.setState({ password })}
                                />
                            </Item>
                            <Button 
                            full rounded success 
                            style={{ marginTop: 20 }}
                            onPress={() => this.SignUp(this.state.email, this.state.password)}
                            >
                                <Text style={styles.textInput}>Cadastrar</Text>
                            </Button>
                            <Button 
                            full rounded
                            style={{ marginTop: 20 }}
                            onPress={() => this.Back()}
                            >
                                <Text style={styles.textInput}>Cancelar</Text>
                            </Button>
                        </Form>
                        <Text style={styles.textInput}>
                            {this.getErrorMessages()}
                        </Text>
                </ScrollView>
            </SafeAreaView>
                    </Container>
        );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#021E33",
    // alignItems: "center",
    justifyContent: "center",
  },
  label: {
    color: "#FFFFFF",
    fontSize: 12
  },
  textInput: {
    color: "#FFFFFF",
    fontSize: 12
  },
  dropdown: {
    borderBottomWidth: 1,
    borderColor: "#FFFFFF"
  },
  childrenContainerStyle: {
    borderWidth: 0,
    alignItems: "flex-start"
  },
  itemFloat: {
      marginLeft: 0
  },
  scrollStyle: {
      margin: 10
  }
});