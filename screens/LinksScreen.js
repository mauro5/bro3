import React, { Component } from 'react';
import { ScrollView, StyleSheet, Button, AsyncStorage } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { StackNavigator } from 'react-navigation';

import firebase from 'firebase';
//import fbConfig from "../env.json";
//const firebaseConfig = fbConfig;
//firebase.initializeApp(firebaseConfig);

export default class LinksScreen extends Component {
    constructor(props) {
      super(props);
    }
  

  SignOut = async () => {
    try {
        firebase.auth().signOut().then(res => {
            firebase.auth().onAuthStateChanged(user => {
              if(this.removeItemValue('user')){
                this.props.navigation.navigate('AuthLoading');
              }
            })    
        }).catch(error =>{
            alert(error.toString(error));
        });
    } catch (error) {
        console.log(error.toString(error));
    }
  };

  removeItemValue(key) {
    try {
        AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
        return false;
    }
  }

  render(){
    return (
      <ScrollView style={styles.container}>
        {/**
         * Go ahead and delete ExpoLinksView and replace it with your content;
         * we just wanted to provide you with some helpful links.
         */}
        <ExpoLinksView />
        <Button onPress={() => this.SignOut()} title="Sair"></Button>
      </ScrollView>
    )
  };
}

LinksScreen.navigationOptions = {
  title: 'Links',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
