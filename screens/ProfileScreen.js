import React, { Component } from 'react';
import { ScrollView, StyleSheet, Button, AsyncStorage, Text } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { StackNavigator } from 'react-navigation';

import firebase from "@firebase/app"
import "firebase/auth"
import "firebase/database"

//import fbConfig from "../env.json";
//const firebaseConfig = fbConfig;
//firebase.initializeApp(firebaseConfig);

export default class ProfileScreen extends Component {
    constructor(props) {
      super(props);
    }
  

    SignOut = async () => {
        try {
            firebase.auth().signOut().then(res => {
                firebase.auth().onAuthStateChanged(user => {
                if(this.removeItemValue('user')){
                    this.props.navigation.navigate('AuthLoading');
                }
                })    
            }).catch(error =>{
                alert(error.toString(error));
            });
        } catch (error) {
            console.log(error.toString(error));
        }
    };

    removeItemValue(key) {
        try {
            AsyncStorage.removeItem(key);
            return true;
        }
        catch(exception) {
            return false;
        }
    }

    _takePicture = () => {
        const cam_options = {
        mediaType: 'photo',
        maxWidth: 1000,
        maxHeight: 1000,
        quality: 1,
        noData: true,
        };
        ImagePicker.launchCamera(cam_options, (response) => {
        if (response.didCancel) {
        }
        else if (response.error) {
        }
        else {
            this.setState({
            imagePath: response.uri,
            imageHeight: response.height,
            imageWidth: response.width,
            })
        }
        })
    }

  render(){
    return (
      <ScrollView style={styles.container}>
        {/**
         * Go ahead and delete ExpoLinksView and replace it with your content;
         * we just wanted to provide you with some helpful links.
         */}
        <Button onPress={() => this.SignOut()} title="Sair"></Button>
      </ScrollView>
    )
  };
}

ProfileScreen.navigationOptions = {
  title: 'Meu Perfil',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: '#fff',
  },
});
