import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Button,
  View,
  AsyncStorage,
  Linking
} from 'react-native';
import MapView, { Marker, MarkerAnimated, Callout } from 'react-native-maps';
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";


import { MonoText } from '../components/StyledText';

import firebase from "@firebase/app"
import "firebase/auth"
import "firebase/database"

const firebaseConfig = {
  apiKey: "AIzaSyD0j4SPMBq8unGQMZUYyIiemJlxrn8WavQ",
  authDomain: "bro3-6ba50.firebaseapp.com",
  databaseURL: "https://bro3-6ba50.firebaseio.com",
  projectId: "bro3-6ba50",
  storageBucket: "bro3-6ba50.appspot.com",
  messagingSenderId: "445843796736",
  appId: "1:445843796736:web:32e0dc16f8a7e1596879e5",
  measurementId: "G-GP4FW6WQW2"
};

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        user: {
          email: "uopa",
        },
        isModalVisible: false,
        mapRegion: null,
        hasLocationPermissions: false,
        locationResult: null,
        coordinate: {
          latitude: 0,
          longitude: 0,
        },
        bro:{},
        markers: []
    };
  }

  componentWillMount() {
    this._retrieveData();
  }

  componentDidMount() {
  }

  callModal(marker){
    console.log(marker);
    this.setState({ bro: marker }, () => {
      this.toggleModal();
    });
  }

  denunciar(){
    alert("Função futura. Aguarde!");
  }

  whatsAppCall(phoneNumber){
    Linking.openURL(`whatsapp://send?text=Fala Bro!&phone=${phoneNumber}`)
  }
  
  phoneCall(phoneNumber){
    Linking.openURL(`tel:${phoneNumber}`)
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  _retrieveData(){
    try {
      AsyncStorage.getItem('user', (err, user) => {
        if (user !== null) {
          this.setState({ user: JSON.parse(user) },() =>{
            this._getLocationAsync();
            // We have data!!
            console.log(user);
          });
        }
      });
    } catch (error) {
      // Error retrieving data
    }
  };

  _handleMapRegionChange = mapRegion => {
    //console.log(mapRegion);
    //this.setState({ mapRegion });
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }
    //console.log(status);

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ locationResult: JSON.stringify(location) });
    
    this.setState({ coordinate: {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    }});
    
    var defaultDatabase = firebase.database();
    var geoRef = defaultDatabase.ref("perfil");
    var uid = this.state.user.uid;
    var query = geoRef.orderByChild('uid').equalTo(uid).limitToFirst(1);
    query.once('value', snap =>{
      if(snap.val() != null){
        var key = Object.keys(snap.val())[0];
        console.log(key);
        firebase.database().ref('perfil/' + key).update({
          coordinate: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude
          },
          email: this.state.user.email,
          uid: this.state.user.uid,
          updatedAt: firebase.database.ServerValue.TIMESTAMP
        });
      }
    });

    this._getBros();

    //console.log(id);
    
    // Center the map on the location we just fetched.
    this.setState({mapRegion: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }});
  };

  _getBros = async () => {
    var defaultDatabase = firebase.database();
    var rootRef = defaultDatabase.ref();
    var geoRef = rootRef.child("perfil");

    geoRef.once('value').then((snapshot) => {
      var snapshotValues = Object.values(snapshot.val());
      var markers = [];
      snapshotValues.forEach(function(childSnapshot){
        //longitude: childSnapshot.coordinate.longitude
        if(childSnapshot.coordinate){
          let marker = {
            coordinate: {
              latitude: childSnapshot.coordinate.latitude,
              longitude: childSnapshot.coordinate.longitude
            },
            email: childSnapshot.email,
            celular: childSnapshot.celular
          }
          //console.log(marker);
          markers.push(marker);
        }
      });
      this.setState({ markers: markers });
    });
  }

  render(){
    return (
      <View style={styles.container}>
        {
          this.state.locationResult === null ?
          <Text>Finding your current location...</Text> :
          this.state.hasLocationPermissions === false ?
            <Text>Location permissions are not granted.</Text> :
            this.state.mapRegion === null ?
            <Text>Map region doesn't exist.</Text> :
            
            <MapView
              style={{ alignSelf: 'stretch', flex: 1}}
              region={this.state.mapRegion}
              onRegionChange={this._handleMapRegionChange}
              >
              {this.state.markers.map((marker,i) => (
                marker.uid !== this.state.user.uid?
                  <Marker
                    key={i}
                    coordinate={marker.coordinate}
                    onPress={() => this.callModal(marker)}
                  />: <Marker
                    key={i}
                    pinColor="navy"
                    coordinate={marker.coordinate}
                  /> 
              ))}
            </MapView>
        }
        <Modal 
        isVisible={this.state.isModalVisible}>
          <View style={styles.modalBro}>
            <Icon
                style={styles.modalClose}
                name="close"
                onPress={this.toggleModal} 
            />
            <View>
              <Icon
                  style={styles.userAvatar}
                  name="account-circle-outline"
              />
              <Text style={styles.text}>{this.state.bro.email}</Text>
            </View>
            <View>
              <TouchableOpacity onPress={() => this.whatsAppCall(this.state.bro.celular)} style={[styles.btn, styles.btnActions]}>
                <Text style={styles.textBtn}>ENVIAR MENSAGEM</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.phoneCall(this.state.bro.celular)} style={[styles.btn, styles.btnActions]}>
                <Text style={styles.textBtn}>LIGAR</Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={() => this.denunciar()} style={[styles.btn, styles.btnDanger]}>
                <Text style={styles.textBtn}>DENUNCIAR</Text>
              </TouchableOpacity>
              <Text style={styles.textDanger}>Este usuário está usando o Bro3 de forma errada?</Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null,
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    const learnMoreButton = (
      <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
        Learn more
      </Text>
    );

    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use
        useful development tools. {learnMoreButton}
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/workflow/development-mode/'
  );
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/workflow/up-and-running/#cant-see-your-changes'
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  userAvatar:{
    fontSize: 100,
    alignSelf: 'center',
    color: "#021E33"
  },
  modalClose:{
    fontSize: 25,
    alignSelf: 'flex-end',
    color: "#021E33"
  },
  modalBro:{
    padding: 20,
    flex: 1,
    height:300,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  modalContent:{
    flexDirection: 'column',
    height: 300,
    alignContent: 'space-between'
  },
  btn: {
    fontSize: 24,
    marginBottom: 10,
    alignItems: 'center',
    color: '#FFFFFF',
    padding: 10
  },
  textBtn: {
    color: "#FFFFFF",
    fontWeight: 'bold'
  },
  btnActions: {
    backgroundColor: "#021E33",
  },
  btnDanger: {
    backgroundColor: "#c4001d",
  },
  text: {
    fontSize: 14,
    color: "#021E33",
    textAlign: 'center'
  },
  textDanger: {
    fontSize: 12,
    color: "#c4001d",
    textAlign: 'center'
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
