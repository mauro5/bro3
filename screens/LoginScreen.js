import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import { Container, Item, Form, Input, Button, Label } from "native-base";
import ValidationComponent from 'react-native-form-validator';

import {
    Image,
    Platform,
    ScrollView,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    AsyncStorage
} from 'react-native';

import firebase from "@firebase/app"
import "firebase/auth"
import "firebase/database"

const firebaseConfig = {
  apiKey: "AIzaSyD0j4SPMBq8unGQMZUYyIiemJlxrn8WavQ",
  authDomain: "bro3-6ba50.firebaseapp.com",
  databaseURL: "https://bro3-6ba50.firebaseio.com",
  projectId: "bro3-6ba50",
  storageBucket: "bro3-6ba50.appspot.com",
  messagingSenderId: "445843796736",
  appId: "1:445843796736:web:32e0dc16f8a7e1596879e5",
  measurementId: "G-GP4FW6WQW2"
};
firebase.initializeApp(firebaseConfig);

export default class LoginScreen extends ValidationComponent {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    componentWillMount() {
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            await AsyncStorage.getItem('user', (err, user) => {
                console.log("_retrieveData");
                console.log(user);
                console.log(typeof user);
                console.log(user !== null);
                if (user !== null) {
                    this.props.navigation.navigate('App');
                }
            });
        } catch (error) {
            // Error retrieving data
        }
    };

    SignUp(){
        console.log("SignUp");
        this.props.navigation.navigate('SignUp');
    };
    
    ForgotPass(){
        console.log("ForgotPass");
        this.props.navigation.navigate('ForgotPass');
    };

    SignIn = (email, password) => {
        console.log("SignIn");
        this.validate({
            email: {email: true},
            password: {required: true},
        });
        console.log(this.isFormValid());
        if(this.isFormValid()){
            try {
                firebase.auth().signInWithEmailAndPassword(email, password).then(res => {
                    firebase.auth().onAuthStateChanged(user => {
                        if (user !== null) {
                            console.log("SignIn");
                            console.log(user);
                            let storageUser = {
                                uid: user.uid,
                                email: user.email,
                                photoURL: user.photoURL
                            }
                            AsyncStorage.setItem('user', JSON.stringify(storageUser));
                            this.props.navigation.navigate('App');
                        }
                    })    
                }).catch(error =>{
                    alert(error.toString(error));
                });
            } catch (error) {
                console.log(error.toString(error));
            }
        }
    };

    render() {
        return (
            <Container style={styles.container}>
                <SafeAreaView>
                    <ScrollView>

                    <Form>
                        <Item floatingLabel style={styles.itemFloat}>
                            <Label style={styles.label}>Email</Label>
                            <Input 
                                autoCapitalize="none" 
                                autoCorrect={false} 
                                style={styles.textInput}
                                onChangeText={email => this.setState({ email })}
                            />
                        </Item>
                        <Item floatingLabel style={styles.itemFloat}>
                            <Label style={styles.label}>Senha</Label>
                            <Input
                                secureTextEntry={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={styles.textInput}
                                onChangeText={password => this.setState({ password })}
                            />
                        </Item>
                        <Button full rounded success 
                        style={{ marginTop: 20 }}
                        onPress={() => this.SignIn(this.state.email, this.state.password)}
                    >
                        <Text style={styles.textInput}>Entrar</Text>
                    </Button>
                        <Button 
                        full rounded
                        style={{ marginTop: 20 }}
                        onPress={() => this.SignUp()}
                    >
                        <Text style={styles.textInput}>Cadastrar</Text>
                    </Button>
                    <TouchableOpacity
                        onPress={() => this.ForgotPass()}
                    >
                        <Text style={styles.textInput}>Esqueci minha senha</Text>
                    </TouchableOpacity>
                    </Form>
                    <Text style={styles.textInput}>
                        {this.getErrorMessages()}
                    </Text>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#021E33",
    // alignItems: "center",
    justifyContent: "center",
    padding: 10
  },
  label: {
    color: "#FFFFFF"
  },
  textInput: {
    color: "#FFFFFF",
    alignSelf: "center",
    margin: 10
  },
  itemFloat: {
      marginLeft: -2
  }
});