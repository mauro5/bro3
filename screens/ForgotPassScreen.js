import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import { Container, Item, Form, Input, Button, Label } from "native-base";
import ValidationComponent from 'react-native-form-validator';

import {
    Image,
    Platform,
    ScrollView,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    AsyncStorage
} from 'react-native';

import firebase from "@firebase/app"
import "firebase/auth"
import "firebase/database"

export default class ForgotPassScreen extends ValidationComponent {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    componentWillMount() {
     
    }

    ForgotPass = (email) => {
        console.log("ForgotPass");
        this.validate({
            email: {email: true}
        });
        console.log(this.isFormValid());
        if(this.isFormValid()){
            try {
                firebase.auth().sendPasswordResetEmail(email).then(res => {
                    alert("Uma nova senha foi enviada para seu email.");
                    this.props.navigation.navigate('AuthLoading');
                }).catch(error =>{
                    alert(error.toString(error));
                });
            } catch (error) {
                console.log(error.toString(error));
            }
        }
    };

    Back(){
        this.props.navigation.navigate('AuthLoading');
    }

    render() {
        return (
            <Container style={styles.container}>
                <SafeAreaView>
                    <ScrollView>

                    <Form>
                        <Item floatingLabel style={styles.itemFloat}>
                            <Label style={styles.label}>Email</Label>
                            <Input 
                                autoCapitalize="none" 
                                autoCorrect={false} 
                                style={styles.textInput}
                                onChangeText={email => this.setState({ email })}
                            />
                        </Item>
                        <Button full rounded success 
                        style={{ marginTop: 20 }}
                        onPress={() => this.ForgotPass(this.state.email)}
                    >
                        <Text>Entrar</Text>
                    </Button>
                    <Button 
                            full rounded
                            style={{ marginTop: 20 }}
                            onPress={() => this.Back()}
                            >
                                <Text style={styles.textInput}>Cancelar</Text>
                            </Button>
                    </Form>
                    <Text style={styles.textInput}>
                        {this.getErrorMessages()}
                    </Text>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#021E33",
    // alignItems: "center",
    justifyContent: "center",
    padding: 20
  },
  label: {
    color: "#FFFFFF"
  },
  textInput: {
    color: "#FFFFFF"
  },
  itemFloat: {
      marginLeft: -2
  }
});