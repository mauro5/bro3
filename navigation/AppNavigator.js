import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import ForgotPassScreen from '../screens/ForgotPassScreen';
import AuthLoadingScreen from '../screens/LoginScreen';



const AppStack = MainTabNavigator;
const AuthStack = createStackNavigator({ SignIn: LoginScreen });

//export default createAppContainer(
//  createSwitchNavigator({
//    // You could add another route here for authentication.
//    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
//    Main: MainTabNavigator,
//  })
//);

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
      SignUp: SignUpScreen,
      ForgotPass: ForgotPassScreen,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);